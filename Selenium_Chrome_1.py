from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from itertools import repeat
import random
import time
import string

text_file = open('english3.txt')
word_list = []

for z in text_file:
    word_list.append(z)

driver = webdriver.Chrome()
driver.get("https://www.ecosia.org/")

y = random.uniform(0.5, 2.5)

while True:
    
    elem = driver.find_element_by_name("q")
    elem.clear()
    elem.send_keys(random.choices(word_list))
    time.sleep(y)
    driver.get("https://www.ecosia.org/")
    elem = driver.find_element_by_name("q")
    elem.clear()
